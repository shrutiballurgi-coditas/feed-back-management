import http from "./http.js"

const studentDetails= async ()=>{
    const tableContainer=document.getElementById("details-area");

    const tableHeaders=`<table class="table" id="table">
    <thead>
    <tr>
    <th class="thead" rowspan="2">studentID</th>
    <th class="thead" rowspan="2">studentName</th>
    <th class="thead" rowspan="2">studentEmail</th>
    </tr>
    </thead>
    <tbody  class="tableRow" id="tableRow">
    </tbody>
    </table>`

    tableContainer.innerHTML=tableHeaders;

    const studentData= await http.http.get(`admin/getStudents`);
    let rowTemplate=``
    const tableRow=document.getElementById("tableRow");
    for (let student of studentData){
        const { studentId,studentName,studentEmail}=student;
        
        
        rowTemplate=rowTemplate+`<tr>
        <td class="td">${studentId}</td>
        <td class="td">${studentName}</td>
        <td class="td">${studentEmail}</td>
        <td class="td"><button>Edit</button>
        <button>Delete</button>
        </td>
        </tr>`

        tableRow.innerHTML=rowTemplate;


    }



}

export default{studentDetails}