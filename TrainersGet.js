import http from "./http.js"
import trainerFeedback from "./trainerFeedback.js"

const trainersGet= async (id,role)=>{
    const tableContainer=document.getElementById("details-area");

    const tableHeaders=`<table class="table" id="table">
    <thead>
    <tr>
    <th class="thead" rowspan="2">trainerId</th>
    <th class="thead" rowspan="2">trainerName</th>
    <th class="thead" rowspan="2">trainerEmail</th>
    <th class="thead" rowspan="2">trainerSalary</th>
    <th class="thead">DomainId</th>
    <th class="thead">DomainName</th>
    <th class="thead">students</th>
    
    </tr>
    </thead>
    <tbody  class="tableRow" id="tableRow">
    </tbody>
    </table>`

    tableContainer.innerHTML=tableHeaders;

    if (role==="Trainer"){
        const trainersData= await http.http.get(`admin/getTrainers/${id}`);
        trainersData(trainersData);
    }
    else{
        const trainersData= await http.http.get("admin/getTrainers");
        console.log(trainersData);
        trainersData(trainersData);
    
    }

}





function trainersData(trainersData){
    let rowTemplate=``
    const tableRow=document.getElementById("tableRow");
    for (let trainer of trainersData){
        const { trainerEmail,trainerId,trainerName,trainerSalary,domain}=trainer;
        console.log(domain);
        
        rowTemplate=rowTemplate+`<tr>
        <td class="td">${trainerId}</td>
        <td class="td">${trainerName}</td>
        <td class="td">${trainerEmail}</td>
        <td class="td">${trainerSalary}</td>
        <td class="td">${domain.domainId}</td>
        <td class="td">${domain.domainName}</td>
        <td class="td"><button>view Students</button></td>
        <td class="td"><button>Edit</button>
        <button>Delete</button>
        </td>
        <td><button class="feedBack">FeedBack<button></td>
        </tr>`

        tableRow.innerHTML=rowTemplate;

        const table=document.getElementById("table");
        table.addEventListener("click",(event)=>{
        const selectedClass=event.target.className;
        console.log(selectedClass);
        if(selectedClass==="feedBack"){
            trainerFeedback(event);
        }
        
    })


        
}
}





export default{trainersGet}