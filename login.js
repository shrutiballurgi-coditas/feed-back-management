
import http from "./http.js"
import dashBoard from "./dashboard.js"
import studentBoard from "./studentBoard.js"
import trainersGet from "./TrainersGet.js"

const loginForm=document.getElementById("form-data");
loginForm.addEventListener("submit", async (e)=>{
    e.preventDefault();
    let emailId=document.getElementById("emailId").value;
    let password=document.getElementById("password").value;
    
   
      let  loginData= {
            "email":emailId,
            "password":password,
        }
        console.log(loginData);
        const data=  await http.http.post("authenticate",loginData);
        console.log(data);
        const role=data.userRole;
        console.log(role);
    
    
    // const data = await response.json();
    
        
        
    if(role==="ADMIN"){
        dashBoard['dashBoard']();
    }
    if(role==="STUDENT"){
        studentBoard['studentBoard'](data.userId);
    }
    if(role==="TRAINER"){
        trainersGet['trainersGet'](data.userId,data.role);
    }
    loginForm.style.display="none";
})




