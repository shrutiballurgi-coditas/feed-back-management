let token=``
class Http {
    #baseUrl = "https://e4fd-103-176-135-84.in.ngrok.io";
    
    async send(endpoint, options = {}, data = null) {
        try {
            options = {
                ...options,
                headers: {
                    Authorization: `Bearer ${token}`,
                    "ngrok-skip-browser-warning": "1234",
                    'Content-Type': 'application/json'
                },
                body: data ? JSON.stringify(data) : null
            }
            
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const responseData = await response.json();

            return responseData;
        } catch (e) {
            throw e;
        }
    }

    async get(endpoint) {
        const ans=await this.send(endpoint);
       
        return ans;
    }

    async delete(endpoint) {
        return await this.send(endpoint, { method: 'DELETE' });
    }

    async post(endpoint, data) {
        console.log("post entered");
        let ans= await this.send(endpoint, { method: 'POST' }, data);
        console.log(ans);
        if(ans.jwtToken)
        {
           token=ans.jwtToken;
        }
        console.log(token);
        return ans
        ;
        }

    async put(endpoint, data) {
        return await this.send(endpoint, { method: 'PUT' }, data)
    }
}

const http = new Http();

export default {http};
